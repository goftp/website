---
date: "2019-06-17T09:54:00+08:00"
title: "server"
weight: 10
toc: false
draft: false
menu: "server"
url: "server"
goimport: "goftp.io/server git https://gitea.com/goftp/server"
gosource: "goftp.io/server https://gitea.com/goftp/server https://gitea.com/goftp/server/tree/master{/dir} https://gitea.com/goftp/server/blob/master{/dir}/{file}#L{line}"
---

# Server - A FTP server framework written by Golang

This is the URL of the import path for [server](http://gitea.com/goftp/server).
