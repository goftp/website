---
date: "2019-06-17T09:54:00+08:00"
title: "ftpd"
weight: 10
toc: false
draft: false
menu: "sidebar"
goimport: "goftp.io/ftpd git https://gitea.com/goftp/ftpd"
gosource: "goftp.io/ftpd https://gitea.com/goftp/ftpd https://gitea.com/goftp/ftpd/tree/master{/dir} https://gitea.com/goftp/ftpd/blob/master{/dir}/{file}#L{line}"
---

# ftpd - A pure go ftp server with web management UI

This is the URL of the import path for [ftpd](http://gitea.com/goftp/ftpd).
